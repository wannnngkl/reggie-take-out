package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.mapper.SetmealDsihMapper;

/**
 * @Author 王开朗
 * @Date 2022/5/30 20:23
 * @Description
 * @Vesion
 **/
public interface SetmealDishService extends IService<SetmealDish> {
}
