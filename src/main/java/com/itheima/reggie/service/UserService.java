package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.User;

/**
 * @Author 王开朗
 * @Date 2022/5/31 15:40
 * @Description
 * @Vesion
 **/
public interface UserService extends IService<User> {
}
