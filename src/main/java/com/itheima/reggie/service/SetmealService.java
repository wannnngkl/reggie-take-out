package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;

import java.util.List;

/**
 * @Author 王开朗
 * @Date 2022/5/25 15:11
 * @Description
 * @Vesion
 **/
public interface SetmealService extends IService<Setmeal> {
    /**
     * 新增套餐，同时需要保存套餐和菜品关联关系
     * @param setmealDto
     */
    public void saveWithDish(SetmealDto setmealDto);


    /**
     * 批量删除套餐和关联数据
     * @param ids
     */
    public void removeWithDish(List<Long> ids);


    /**
     * 批量停售套餐
     * @param status
     * @param ids
     */
    public void updateStatus(Integer status, List<Long> ids);
}
