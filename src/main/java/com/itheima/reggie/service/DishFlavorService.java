package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.DishFlavor;

/**
 * @Author 王开朗
 * @Date 2022/5/26 16:21
 * @Description
 * @Vesion
 **/
public interface DishFlavorService extends IService<DishFlavor> {
}
