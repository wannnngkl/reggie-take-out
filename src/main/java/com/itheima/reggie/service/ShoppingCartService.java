package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.ShoppingCart;

/**
 * @Author 王开朗
 * @Date 2022/6/1 15:50
 * @Description
 * @Vesion
 **/
public interface ShoppingCartService extends IService<ShoppingCart> {
    /**
     * 清理购物车
     */
    public void clean();
}
