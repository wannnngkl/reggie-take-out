package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Employee;

/**
 * @Author 王开朗
 * @Date 2022/5/23 16:40
 * @Description
 * @Vesion
 **/
public interface EmployeeService extends IService<Employee> {
}
