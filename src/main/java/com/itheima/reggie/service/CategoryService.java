package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Category;

/**
 * @Author 王开朗
 * @Date 2022/5/25 13:51
 * @Description
 * @Vesion
 **/
public interface CategoryService extends IService<Category> {
    public void remove(Long id);
}
