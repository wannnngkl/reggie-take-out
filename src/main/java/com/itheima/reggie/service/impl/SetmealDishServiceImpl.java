package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.mapper.SetmealDsihMapper;
import com.itheima.reggie.service.SetmealDishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @Author 王开朗
 * @Date 2022/5/30 20:24
 * @Description
 * @Vesion
 **/
@Slf4j
@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDsihMapper, SetmealDish> implements SetmealDishService {
}
