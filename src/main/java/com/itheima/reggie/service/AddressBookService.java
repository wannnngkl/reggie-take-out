package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.AddressBook;

/**
 * @Author 王开朗
 * @Date 2022/6/1 10:19
 * @Description
 * @Vesion
 **/
public interface AddressBookService extends IService<AddressBook> {
}
