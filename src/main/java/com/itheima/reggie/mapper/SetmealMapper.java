package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Setmeal;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 王开朗
 * @Date 2022/5/25 15:10
 * @Description
 * @Vesion
 **/

@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
