package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Dish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 王开朗
 * @Date 2022/5/25 15:09
 * @Description
 * @Vesion
 **/

@Mapper
public interface DishMapper extends BaseMapper<Dish> {

}
