package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 王开朗
 * @Date 2022/5/30 20:21
 * @Description
 * @Vesion
 **/
@Mapper
public interface SetmealDsihMapper extends BaseMapper<SetmealDish> {
}
