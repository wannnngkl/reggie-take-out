package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 王开朗
 * @Date 2022/5/31 15:40
 * @Description
 * @Vesion
 **/

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
