package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.DishFlavor;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 王开朗
 * @Date 2022/5/26 16:20
 * @Description
 * @Vesion
 **/
@Mapper
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {
}
