package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 王开朗
 * @Date 2022/5/25 13:49
 * @Description
 * @Vesion
 **/

@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
