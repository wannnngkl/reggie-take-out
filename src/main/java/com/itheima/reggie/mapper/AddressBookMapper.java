package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.AddressBook;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 王开朗
 * @Date 2022/6/1 10:18
 * @Description
 * @Vesion
 **/
@Mapper
public interface AddressBookMapper extends BaseMapper<AddressBook> {
}
