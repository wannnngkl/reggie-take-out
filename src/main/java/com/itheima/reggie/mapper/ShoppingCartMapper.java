package com.itheima.reggie.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 王开朗
 * @Date 2022/6/1 11:54
 * @Description
 * @Vesion
 **/
@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {
}
