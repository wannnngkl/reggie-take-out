package com.itheima.reggie.common;

/**
 * @Author 王开朗
 * @Date 2022/5/25 15:29
 * @Description
 * @Vesion
 **/

/**
 * 自定义业务异常类
 */
public class CustomException extends RuntimeException{
    public CustomException(String message){
        super(message);
    }
}
