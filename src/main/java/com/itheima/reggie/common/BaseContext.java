package com.itheima.reggie.common;

/**
 * @Author 王开朗
 * @Date 2022/5/25 13:04
 * @Description 基于ThreadLocal封装工具类，用户保存和获取当前登录用户id
 * @Vesion
 **/
public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * 设置值
     * @param id
     */
    public static void setCurrentId(Long id){
        threadLocal.set(id);
    }

    /**
     * 获取值
     * @return
     */
    public static Long getCurrentId(){
        return threadLocal.get();
    }
}
